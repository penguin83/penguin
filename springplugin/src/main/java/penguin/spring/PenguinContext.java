package penguin.spring;

import org.springframework.context.ApplicationContext;

/**
 * Created on 15/7/4 下午8:33
 *
 * @author 王建华(penguin83@126.com)
 */
public class PenguinContext {

    private static ApplicationContext applicationContext;

    public static void initApplicationContext(ApplicationContext applicationContext) {
        PenguinContext.applicationContext = applicationContext;
    }

    public static Object getBeanById(String serviceId){
        return applicationContext.getBean(serviceId);
    }
}
