package penguin.spring; /**
 * Created on 15/7/4 下午8:16
 * @author 王建华(penguin83@126.com)
 */

import penguin.rpc.RpcHandler;
import penguin.rpc.dto.RpcDTO;

import java.lang.reflect.Method;

public class SpringContextHandler implements RpcHandler {


    public Object invoke(RpcDTO rpcDTO) throws Exception {

       Object object = PenguinContext.getBeanById(rpcDTO.getServiceName());

        Class clazz = object.getClass();

        Object[] params = rpcDTO.getParams();
        Class[] paramClass = new Class[params.length];

        for(int i = 0;i<paramClass.length;i++){
            paramClass[i] = params[i].getClass();
        }

        Method method = clazz.getDeclaredMethod(rpcDTO.getMethodName(),paramClass);

        return   method.invoke(object,rpcDTO.getParams());
    }
}
