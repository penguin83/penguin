package penguin.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import penguin.rpc.RpcClient;

/**
 * 在wen.xml中配置此listener
 *
 * Created on 15/7/4 下午8:25
 *
 * @author 王建华(penguin83@126.com)
 */
public class PenguinContextListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PenguinContextListener.class);

    public void onApplicationEvent(ContextRefreshedEvent event) {

        //防止重复执行。
        if(event.getApplicationContext().getParent() == null){
            LOGGER.debug("PenguinContextListener=>onApplicationEvent");
            PenguinContext.initApplicationContext(event.getApplicationContext());

        }

    }
}
