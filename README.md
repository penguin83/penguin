Penguin 项目

基于 spring + netty 实现的简洁分布式框架

使用方式：

一，简单的直连方式

   1.server端：
   
         只需要在 spring的配置文件中配置 rpcServer 即可，注意端口不能被其他应用占用
         
        <bean class="penguin.spring.PenguinContextListener"/>
          <bean id="springContextHandler" class="penguin.spring.SpringContextHandler"/>
          <bean id="rpcServer" class="penguin.rpc.RpcServer" init-method="init">
              <property name="port" value="7001"/>
              <property name="rpcHandler"  ref="springContextHandler"/>
          </bean>
          
  2.client端：
          首先需要实现client端的调用类，并且要继承 penguin.rpc.RpcClient
          然后只需要指定好server端对应service的 名称，方法名和参数，即可进行调用
          
        public class RemoteClient1 extends RpcClient {
          
              public String test1(String name){
                  String serviceName = "aService"; 
                  String methodName = "testMethod1";
                  String param1 = "name";
                  return (String)this.execSync(serviceName,methodName,param1);
              }
              
              public String test1(String name){
                    String serviceName = "aService"; 
                    String methodName = "testMethod2";
                    String param1 = "name";
                    String param2 = "password";
                    return (String)this.execSync(serviceName,methodName,param1,param2);
              }
          }
          
          然后只需要再在spring的配置文件中进行配置，并且指定 被调用的server端的ip和端口即可
          框架会自动从第一个地址进行连接尝试，当连接失败后会自动连接第二个地址
        <bean id="remoteClient1" class="demo.remote.RemoteClient1" init-method="initDirect">
               <property name="addressList">
                   <list>
                       <value>127.0.0.1:7001</value>
                       <value>127.0.0.1:7002</value>
                   </list>
               </property>
           </bean>
           


二，通过注册中心进行调用

    1.注册中心 registerServer：
       首先 找到 penguin中的 registry项目，可以其spring配置文件 application-config.xml中 设置自己希望的端口号，例如：
            <bean class="penguin.spring.PenguinContextListener"/>
              <bean id="springContextHandler" class="penguin.spring.SpringContextHandler"/>
              <bean   class="penguin.rpc.RpcServer" init-method="init">
                  <property name="port" value="7000"/>
                  <property name="rpcHandler"  ref="springContextHandler"/>
              </bean>
      然后在tomcat中启动  penguin中的 registry项目
>      
    2.server端：
           在server端加入如下配置，目前 注册中心的地址只支持单点，即一个机器，后期会改造为支持多个注册中心的机器，以便于更好的高可用性
          <bean class="penguin.spring.PenguinContextListener"/>
            <bean id="springContextHandler" class="penguin.spring.SpringContextHandler"/>
            <bean  id="rpcServer" class="penguin.rpc.RpcServer" init-method="init">
                <property name="port" value="7001"/>
                <property name="rpcHandler"  ref="springContextHandler"/>
            </bean>
        
            <!-- 注册中心配置 -->
            <bean id="registerClient" class="penguin.rpc.RegisterClient" init-method="init">
                <!-- server端项目的项目名称，以便于注册到注册中心 -->
                <property name="projectName" value="demo1"/>
                <property name="rpcServer"  ref="rpcServer"/>
                <!-- 目前只支持一个ip地址 -->
                <property name="registerAddressList">
                    <list>
                        <value>127.0.0.1:7000</value>
                    </list>
                </property>
            </bean>
>    
    3.client端：
                 public class RemoteClient1 extends RpcClient {
                        public String test1(String name){
                            String serviceName = "aService"; 
                            String methodName = "testMethod1";
                            String param1 = "name";
                            return (String)this.execSync(serviceName,methodName,param1);
                        }
                        public String test1(String name){
                              String serviceName = "aService"; 
                              String methodName = "testMethod2";
                              String param1 = "name";
                              String param2 = "password";
                              return (String)this.execSync(serviceName,methodName,param1,param2);
                        }
                    }
          
           <!-- 注册中心配置 -->
            <bean id="registerClient" class="penguin.rpc.RegisterClient" init-method="init">
                <property name="projectName" value="demo1"/>
                <property name="rpcServer"  ref="rpcServer"/>
                <property name="registerAddressList">
                    <list>
                        <value>127.0.0.1:7000</value>
                    </list>
                </property>
            </bean>
        
            <!-- 通过注册中心获取项目地址-->
            <bean id="remoteClient1" class="demo.remote.RemoteClient1" init-method="init">
                <!-- 该client希望调用的项目名称 -->
                <property name="remoteProjectName" value="demo1"/>
                <property name="registerClient" ref="registerClient"/>
            </bean>
            
>         注意：
         1.在client的配置文件中设置好希望调用的项目名称后，框架在项目启动后会自动与注册中心进行连接，并且通过希望调用的项目名称获取其在注册中心中注册的ip地址列表
         2.client端在获取远程项目的地址列表之后，会挨个地址列表进行尝试连接，直到与其中一个连接成功
         3.在client和server连接成功之后，就可以进行远程方法调用了，方法见上面
         4.server与注册中心之间会进行心跳通讯，当心跳超时，server与注册中心断开后，注册中心会认为这个server已经宕机，并且从地址列表中移除该server
         5.client与注册中心之间会进行心跳通讯，每次心跳注册中心都会将最新的server地址列表给client，以便于client进行访问
         5.如果注册中心宕机，也不会影响项目正常运行
           client本地会保留最后的server端地址列表，以便于继续和server端进行通讯，直到注册中心恢复
       
           
>       后续开发计划：
       1.添加注册中心的页面显示，展现当前已注册的server信息 和每个client需要获取的server项目信息
       2.添加会话信息，每次调用链都视为一次会话，然后可以在这次会话中保存类似的session信息，以便于每个节点进行使用
       3.添加监控中心，可以统计和展现所有的方法调用链信息以及时间       
           
