package penguin.common;

/**
 * Created on 15/7/3 下午2:35
 *
 * @author 王建华(penguin83@126.com)
 */
public class Constants {
	public static final int DEFAULT_IO_THREADS = Runtime.getRuntime().availableProcessors() + 1;
    public static final int CONNECT_TIME_OUT = 10 * 1000;
    public static final int EXECUTE_TIME_OUT = 10 * 1000;
    // default buffer size is 8k.
    public static final int     DEFAULT_BUFFER_SIZE                = 8 * 1024;
    public static final int     MAX_BUFFER_SIZE                    = 16 * 1024;
    public static final int     MIN_BUFFER_SIZE                    = 1 * 1024;
    public static final int HEART_TIME = 30;
    public static final int HEART_TIME_OUT = 90;

    public static final int HEART_TIME_REIGSTER = 1000;
    public static final int RETRY_TIME = 1000;
}
