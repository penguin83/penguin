package penguin.exception;

/**
 * Created on 15/7/10 下午3:30
 *
 * @author 王建华(penguin83@126.com)
 */
public class PenguinException extends  Exception {

    public PenguinException(String message){
        super(message);
    }

    public PenguinException(String message, Exception e) {
        super(message,e);
    }
}
