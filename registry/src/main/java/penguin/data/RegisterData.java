package penguin.data;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created on 15/7/10 下午10:20
 *
 * @author 王建华(penguin83@126.com)
 */
public class RegisterData {

    private RegisterData(){}

    private static RegisterData registerData = new RegisterData();

    public static RegisterData inst(){
        return registerData;
    }

    //project Set<address>
    private Map<String,Set<String>> projectAddressMap = new ConcurrentHashMap<String, Set<String>>();
    //address project
    private Map<String,String> addrProjectMap = new ConcurrentHashMap<String,String>();

    public void registerProject(String projectName, String projectAddress) {

        addrProjectMap.put(projectAddress,projectName);

        if(projectAddressMap.containsKey(projectName)){
            projectAddressMap.get(projectName).add(projectAddress);
        }
        else{

            Set<String> projectAddressSet = new HashSet<String>();
            projectAddressSet.add(projectAddress);
            projectAddressMap.put(projectName,projectAddressSet);
        }
    }

    public List<String> getProjectAddressList(String projectName) {
        Set<String> projectAddressSet = projectAddressMap.get(projectName);
        if(projectAddressSet == null){
            projectAddressSet = new HashSet<String>();
        }
        return new ArrayList<String>(projectAddressSet);
    }

    public void removeByAddr(String addr) {

        String projectName = addrProjectMap.remove(addr);
        projectAddressMap.get(projectName).remove(addr);

    }
}
