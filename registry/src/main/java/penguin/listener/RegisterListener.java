package penguin.listener;

import org.springframework.beans.factory.annotation.Autowired;
import penguin.data.RegisterData;
import penguin.rpc.RpcListener;

/**
 * Created on 15/7/11 下午6:27
 *
 * @author 王建华(jh.wang05@zuche.com)
 */
public class RegisterListener implements RpcListener {


    public void connected(String hostAddress, int port) {

    }

    public void disConnected(String hostAddress, int port) {
        String addr = hostAddress+":"+port;
        RegisterData.inst().removeByAddr(addr);
    }
}
