import penguin.rpc.RpcClient;
import penguin.rpc.RpcServer;

/**
 * Created on 15/7/9 下午11:45
 *
 * @author 王建华(penguin83@126.com)
 */
public class TestRpcClient {

    public static void  main(String[] args) throws InterruptedException {
        RpcClient rpcClient = new RpcClient();
        rpcClient.setCurrentHost("127.0.0.1");
        rpcClient.setCurrentPort(9000);
        rpcClient.initDirect();

        Thread.sleep(9000);
    }

}
