import penguin.rpc.RpcServer;

/**
 * Created on 15/7/9 下午11:45
 *
 * @author 王建华(penguin83@126.com)
 */
public class TestRpcServer {

    public static void  main(String[] args) throws InterruptedException {
        RpcServer rpcServer = new RpcServer();
        rpcServer.setPort(9000);
        rpcServer.init();

        Thread.sleep(999999);
    }

}
