package penguin.rpc;

import penguin.rpc.dto.RpcDTO;

import java.lang.reflect.InvocationTargetException;

/**
 * Created on 15/7/4 下午8:13
 *
 * @author 王建华(penguin83@126.com)
 */
public interface RpcHandler {

    Object invoke(RpcDTO rpcDTO) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, Exception;

}
