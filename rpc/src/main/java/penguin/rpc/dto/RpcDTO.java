package penguin.rpc.dto;

import java.io.Serializable;

/**
 * Created on 15/7/4 上午10:56
 *
 * @author 王建华(penguin83@126.com)
 */
public class RpcDTO implements Serializable {

    private String serviceName = "";
    private String methodName = "";
    private Object[] params;

    public RpcDTO(String serviceName, String methodName, Object... params) {
        this.serviceName = serviceName;
        this.methodName = methodName;
        this.params = params;
        if(params == null){
            this.params = new Object[0];
        }
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public String toString(){
        return "serviceName:"+serviceName+" methodName:"+methodName;
    }
}
