package penguin.rpc;

/**
 * Created on 15/7/11 下午6:24
 *
 * @author 王建华(jh.wang05@zuche.com)
 */
public interface RpcListener {

    void connected(String hostAddress, int port);
    void disConnected(String hostAddress, int port);
}
