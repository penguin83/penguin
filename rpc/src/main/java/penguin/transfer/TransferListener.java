package penguin.transfer;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;

/**
 * Created on 15/7/4 下午1:18
 *
 * @author 王建华(penguin83@126.com)
 */
public interface TransferListener {

     void connected(String hostAddress, int port);

     void disConnected(String hostAddress, int port);

     void receiveMessage(Channel channel,Object message);

     void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e);
}
