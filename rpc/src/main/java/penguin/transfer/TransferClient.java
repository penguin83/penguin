package penguin.transfer;

/**
 * 远程调用客户端
 *
 * Created on 15/7/2 上午9:23
 *
 * @author 王建华(penguin83@126.com)
 */
public interface TransferClient {

     void init(String host,int port,TransferListener transferListener);

     void connect();

     void close();

     Object sendSync(Object object) throws Exception;

     boolean isConnected();
}
