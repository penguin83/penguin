package penguin.transfer.dto;


import java.io.Serializable;
import java.util.UUID;

/**
 * Created on 15/7/4 上午9:41
 *
 * @author 王建华(penguin83@126.com)
 */
public class TransferDTO implements Serializable {

    private  String uuid;
    private  String token;
    private  String type;
    private  Object object;
    private  boolean hasException=false;

    public static TransferDTO buildMessage(Object object){

        TransferDTO transferDTO = new TransferDTO();

        transferDTO.uuid = UUID.randomUUID().toString();
        transferDTO.object = object;
        transferDTO.type = "call";

        return transferDTO;
    }

    public static TransferDTO buildHeart(){

        TransferDTO transferDTO = new TransferDTO();
        transferDTO.type = "heart";
        return transferDTO;
    }



    private TransferDTO(){
    }


    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isHasException() {
        return hasException;
    }

    public void setHasException(boolean hasException) {
        this.hasException = hasException;
    }
}
