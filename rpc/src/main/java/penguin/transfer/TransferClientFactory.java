package penguin.transfer;

import penguin.transfer.netty.client.NettyClient;

/**
 * 远程调用客户端
 *
 * Created on 15/7/2 上午9:23
 *
 * @author 王建华(penguin83@126.com)
 */
public class TransferClientFactory {


    private TransferClientFactory(){}


    public synchronized static TransferClient buildNetty(String host, int port, TransferListener transferListener){

          NettyClient nettyClient = new NettyClient();
          nettyClient.init(host,port,transferListener);

        return nettyClient;
    }

}
