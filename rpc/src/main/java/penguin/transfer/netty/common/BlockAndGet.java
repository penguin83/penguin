package penguin.transfer.netty.common;

import penguin.common.Constants;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Created on 15/7/3 下午10:08
 *
 * @author 王建华(penguin83@126.com)
 */
public class BlockAndGet {

    private CountDownLatch countDownLatch;
    private Object object;
    private String exception;

    public BlockAndGet(){
        countDownLatch = new CountDownLatch(1);
    }

    public void lock() throws InterruptedException {
        countDownLatch.await(Constants.EXECUTE_TIME_OUT, TimeUnit.MILLISECONDS);
    }

    public void unlock() throws InterruptedException {
        countDownLatch.countDown();
    }

    public void setAndUnlock(Object object){
        this.object = object;
        countDownLatch.countDown();

    }

    public void unlockAndException(String exception){
        this.exception = exception;
        countDownLatch.countDown();

    }

    public Object getObject() {
        return object;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }
}
