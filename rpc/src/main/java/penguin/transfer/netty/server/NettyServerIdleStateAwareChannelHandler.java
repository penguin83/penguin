package penguin.transfer.netty.server;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import org.jboss.netty.handler.timeout.ReadTimeoutException;

/**
 *  * socket 空闲 处理 类
 * 当读空闲足够时间的时候会调用此方法抛出超时异常
 * 当读空闲足够时间的时候会调用此方法抛出超时异常
 * 用于服务器端
 * <br/> Created on 15/1/12 上午10:27
 *
 * @author 王建华(penguin83@126.com)
 */
public class NettyServerIdleStateAwareChannelHandler extends IdleStateAwareChannelHandler {


    @Override
    public void channelIdle(ChannelHandlerContext ctx,IdleStateEvent e) throws Exception{

        super.channelIdle(ctx,e);

        switch(e.getState()){
            case WRITER_IDLE: {
                throw new ReadTimeoutException("写空闲超时，抛出超时异常。"+e.getChannel().getId());
            }
            case READER_IDLE: {
                throw new ReadTimeoutException("读空闲超时，抛出超时异常。"+e.getChannel().getId());
            }

        }

    }

}
