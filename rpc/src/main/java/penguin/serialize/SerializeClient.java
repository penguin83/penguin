package penguin.serialize;

import java.io.Serializable;

/**
 * Created on 15/7/2 上午9:23
 *
 * @author 王建华(penguin83@126.com)
 */
public abstract class SerializeClient {

     public abstract byte[] serialize(Object serializable);

     public abstract Object deserialize(byte[] bytes);
}
