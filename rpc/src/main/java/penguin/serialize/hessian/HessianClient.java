package penguin.serialize.hessian;

import com.caucho.hessian.io.HessianSerializerInput;
import com.caucho.hessian.io.HessianSerializerOutput;
import penguin.serialize.SerializeClient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created on 15/7/3 上午10:29
 *
 * @author 王建华(penguin83@126.com)
 */
public class HessianClient extends SerializeClient {


    public byte[] serialize(Object serializable){

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        HessianSerializerOutput hessianSerializerOutput = new HessianSerializerOutput(bos);
        try {
            hessianSerializerOutput.writeObjectImpl(serializable);
            hessianSerializerOutput.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return bos.toByteArray();
    }


    public Object deserialize(byte[] bytes){
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        HessianSerializerInput hessianSerializerInput = new HessianSerializerInput(inputStream);

        try {
            return  hessianSerializerInput.readObject();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

}
