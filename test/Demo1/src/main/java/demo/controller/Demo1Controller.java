package demo.controller;

import demo.remote.RemoteClient1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created on 15/7/4 下午10:35
 *
 * @author 王建华(penguin83@126.com)
 */
@Controller
@RequestMapping("/demo1")
public class Demo1Controller {

    @Resource
    private RemoteClient1 remoteClient1;

    @ResponseBody
    @RequestMapping("test1")
    public String test1(String name) throws Exception {
        return remoteClient1.test1(name);
    }

    @ResponseBody
    @RequestMapping("test11")
    public String test1(String name,String value) throws Exception {
        return remoteClient1.test1(name,value);
    }

}
