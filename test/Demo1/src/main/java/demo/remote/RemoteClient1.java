package demo.remote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import penguin.rpc.RpcClient;

import penguin.rpc.RpcClient;
import penguin.rpc.RpcServer;


/**
 * Created on 15/7/4 下午10:35
 *
 * @author 王建华(penguin83@126.com)
 */
public class RemoteClient1 extends RpcClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(RpcServer.class);
    public String test1(String name) throws Exception {
        String serviceName = "aService";
        String methodName = "test3";
        String s =  (String)this.execSync(serviceName,methodName,name);
        LOGGER.debug("test1:" + s);
        return s;
    }

    public String test1(String name,String value) throws Exception {
        String serviceName = "aService";
        String methodName = "test1";
        String s = (String)this.execSync(serviceName,methodName,name,value);
        LOGGER.debug("test2:" + s);
        return s;
    }

}
