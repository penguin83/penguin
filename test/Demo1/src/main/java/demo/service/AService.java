package demo.service;

import org.springframework.stereotype.Service;

/**
 * Created on 15/7/4 下午10:35
 *
 * @author 王建华(penguin83@126.com)
 */
@Service("aService")
public class AService {

    public String test1(String name){
        return "AService#test1:"+name;
    }

    public String test1(String name,String value){
        return "AService#test2:"+name+" "+value;
    }
}
